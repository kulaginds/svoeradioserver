<?php

	define("API_ROOT", dirname(__FILE__));
	define("CORE_ROOT", API_ROOT."/core");

	header("Content-type: text/plain; charset=utf-8");

	set_time_limit(0);

	include_once CORE_ROOT."/config.php";
	include_once CORE_ROOT."/functions.php";

	$db = connect_database();

	try {
		// пометка об удалении
		$old_notifications = $db->query("SELECT user_id FROM notifications WHERE sent=1 AND (date + INTERVAL 30 DAY) <= NOW() AND user_id NOT IN (SELECT user_id FROM notifications WHERE message_code='STATION_REMOVE')");

		if ($old_notifications->num_rows > 0) {
			$old_notifications = $old_notifications->fetch_all(MYSQLI_ASSOC);
			$users = array();
			foreach ($old_notifications as $notify)
				$users[] = (int)$notify['user_id'];

			unset($old_notifications);

			$sql = "INSERT INTO notifications(user_id,message_code) VALUES";
			for ($i=0; $i<count($users); $i++)
				$sql .= (($i == 0)?"":",")."(".$users[$i].", '".NOTIFY_STATION_REMOVE."')";
			$db->query($sql);

			$sql = "UPDATE stations SET deleted_date = NOW(), removed=1 WHERE runned=0 AND banned=0 AND removed=0 AND freezy=0 AND user_id IN (".implode(",", $users).")";
			$db->query($sql);
		}

		// удаление данных об удаленных станциях
		$deleted_stations = $db->query("SELECT id FROM stations WHERE (deleted_date + INTERVAL 30 DAY) <= NOW()");

		if ($deleted_stations->num_rows > 0) {
			$deleted_stations = $deleted_stations->fetch_all(MYSQLI_ASSOC);
			$station_id = 0;

			$stations = $db->prepare("DELETE FROM stations WHERE id=?");
			$stations->bind_param("i", $station_id);

			$stats = $db->prepare("UPDATE stats SET hide=1 WHERE station_id=?");
			$stats->bind_param("i", $station_id);

			$rating = $db->prepare("UPDATE rating SET hide=1 WHERE station_id=?");
			$rating->bind_param("i", $station_id);

			$queues = $db->prepare("UPDATE queues SET hide=1 WHERE station_id=?");
			$queues->bind_param("i", $station_id);

			foreach ($deleted_stations as $station) {
				$station_id = $station["id"];

				$stations->execute();
				$stats->execute();
				$rating->execute();
				$queues->execute();
			}

			$db->query("OPTIMIZE TABLE stations");
		}

		// удаление неудаленных треков из таблицы queues у несуществующих станций
		$queue_stations = $db->query("SELECT DISTINCT station_id AS id FROM queues");
		$all_stations = $db->query("SELECT id FROM stations");
		$all = array();

		while ($row = $all_stations->fetch_object())
		{
			$all[] = $row->id;
		}

		$id = 0;
		$rm = $db->prepare("UPDATE queues SET hide=1 WHERE station_id=?");
		$rm->bind_param("i", $id);

		while ($row = $queue_stations->fetch_object())
		{
			$id = $row->id;

			if (FALSE === in_array($id, $all))
				$rm->execute();
		}

		// очистка записей с hide=1
		$db->query("DELETE FROM stats WHERE hide=1");
		$db->query("DELETE FROM rating WHERE hide=1");
		$db->query("DELETE FROM queues WHERE hide=1");
		// оптимизация таблиц
		$db->query("OPTIMIZE TABLE stats");
		$db->query("OPTIMIZE TABLE rating");
		$db->query("OPTIMIZE TABLE queues");
		$db->query("OPTIMIZE TABLE notifications");

		// обновление сортировки каждое воскресенье
		if ((date("N") == 7) || DEBUG) {
			$stations = $db->query("SELECT station_id, COUNT(DISTINCT user_id) AS visitors FROM stats WHERE YEARWEEK(date, 1) = YEARWEEK(CURDATE(), 1) GROUP BY station_id");
			$stations = $stations->fetch_all(MYSQLI_ASSOC);
			$count = 0;

			foreach ($stations as $station) 
				$count += $station["visitors"];

			$db->query("UPDATE stations SET sort=0");

			if ($count > 0) {
				$sort = 0;
				$station_id = 0;

				$query = $db->prepare("UPDATE stations SET sort=? WHERE id=?");
				$query->bind_param("di", $sort, $station_id);

				foreach ($stations as $station) {
					$sort = $station["visitors"] / $count;
					$station_id = $station["station_id"];
					$query->execute();
				}
			}
		}

		// отправка оповещений ведущим
		$query = $db->query("SELECT DISTINCT user_id, message_code FROM notifications WHERE sent=0");
		if ($query->num_rows > 0) {
			$notifications = $query->fetch_all(MYSQLI_ASSOC);
			$groups = array();

			foreach ($notifications as $notify)
				$groups[$notify['message_code']][] = $notify['user_id'];

			unset($notifications);

			foreach ($groups as $message_code => $users) {
				$users = array_chunk($users, VK_NOTIFY_COUNT);
				for ($i=0; $i<count($users); $i++) {
					$ids = implode(",", $users[$i]);
					$result = vk_query("secure.sendNotification", array(
						'user_ids' => $ids,
						'message' => $NOTIFY_MESSAGES[$message_code],
					));

					if (array_key_exists('response', $result)) {
						$db->query("UPDATE notifications SET sent=1 WHERE message_code='".$message_code."' AND user_id IN (".$ids.")");
					}
				}
			}
		}

	} catch (mysqli_sql_exception $e) {
		print "mysql_exception: ".$e->getMessage()." on #".$e->getLine();
	}

	$db->close();

	print "1";