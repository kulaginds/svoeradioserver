<?php

	define("API_ROOT", dirname(__FILE__));
	define("CORE_ROOT", API_ROOT."/core");
	
	header("Content-type:application/json; charset=utf-8");

	include_once CORE_ROOT."/config.php";
	include_once CORE_ROOT."/functions.php";

	if (!DEBUG) {
		set_error_handler("error_handler");
		set_exception_handler("exception_handler");
	} else
		ini_set("display_errors", 1);

	$db = connect_database();

	if (($_SERVER["REQUEST_METHOD"] != "POST") || !isset($_POST["method"])
		|| !preg_match("/^[a-z]+\.[a-z]+$/i", $_POST["method"])) 
		print_error(ERR_UNKNOWN_METHOD);

	$method = explode(".", $_POST["method"]);
	$method = CORE_ROOT."/methods/".$method[0]."/".$method[1].".php";

	if (!is_file($method))
		print_error(ERR_UNKNOWN_METHOD);

	if (!isset($_POST["api_id"], $_POST["auth_key"], $_POST["viewer_id"])
		|| ($_POST["api_id"] <= 0) || (strlen($_POST["auth_key"]) != 32)
		|| ($_POST["viewer_id"] <= 0)
		|| !check_vk_auth($_POST["api_id"], $_POST["auth_key"], $_POST["viewer_id"]))
		print_error(ERR_AUTH);

	include $method;
?>
