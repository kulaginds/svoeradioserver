<?php if (!defined("API_ROOT")) exit("Hacking attempt!");
	
	// параметры для подключения к базе данных
	define("DB_HOST", "localhost");
	define("DB_USER", "root");
	define("DB_PASS", "mysecurepassword");
	define("DB_NAME", "svoeradio");
	define("DB_CHARSET", "utf8");