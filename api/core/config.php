<?php if (!defined("API_ROOT")) exit("Hacking attempt!");

	// флаг отладки
	define("DEBUG", 0);

	// подключаем конфиг с параметрами подключения к БД
	include_once CORE_ROOT."/db_config.php";

	// подключаем конфиг с параметрами приложений ВК
	include_once CORE_ROOT."/vk_config.php";

	// станция по умолчанию, если нет ничего нормального
	define("DEFAULT_STATION_ID", 1);

	// максимальное количество треков в очереди
	define("MAX_QUEUE_SIZE", 300);

	// минимальный год статистики
	define("MIN_STATS_YEAR", 2013);

	// минимальный размер названия станции
	define("STATION_MIN_LENGTH", 3);

	// максимальный размер названия станции
	define("STATION_MAX_LENGTH", 15);

	// количество станций при поиске
	define("MAX_SEARCH_COUNT", 50);

	/*
		Ошибки и сообщения
	*/
	// коды ошибок:
	define("SYSTEM_ERROR", 100);
	define("API_ERROR_CRITICAL", 200);
	define("API_ERROR", 300);

	// сообщения ошибок:
	define("ERR_NO_MYSQLND", "Нет mysqlnd расширения на PHP.");
	define("ERR_INTERNAL", "Внутренняя ошибка сервера.");
	define("ERR_CONN_DB", "Не удалось подключиться к базе данных.");
	define("ERR_UNKNOWN_METHOD", "Метод не указан, или указан неизвестный метод.");
	define("ERR_AUTH", "Ошибка авторизации клиента.");
	define("ERR_MISS_PARAM", "Невалидные данные параметра: ");
	define("ERR_STATION_NOT_EXISTS", "Станции не существует либо она заблокирована.");
	define("ERR_BROADCAST_STOP", "Трансляция остановлена.");
	define("ERR_NO_QUEUE_TRACKS", "Нет треков в очереди для запуска трансляции.");
	define("ERR_STATION_BANNED", "Станция заблокирована.");
	define("ERR_STATION_REMOVED", "Станция удалена.");
	define("ERR_PERMISSIONS_DENIED", "Доступ запрещен.");
	define("ERR_QUEUE_EXCEEDS_AMOUNT", "Ошибка сохранения: количество треков в очереди превышает значение ".MAX_QUEUE_SIZE.".");
	define("ERR_QUEUE_NOT_ALL_SAVED", "Не удалось сохранить все треки в очередь (возможно сохранена часть треков).");
	define("ERR_QUEUE_SAVE", "Не удалось сохранить треки в очередь.");
	define("ERR_PREV_VOTED", "Вы уже голосовали за выбранную станцию.");
	define("ERR_MISS_LIM", "Выбран неверный промежуток времени.");
	define("ERR_GROUP_ENGAGED", "Выбранная группа занята.");
	define("ERR_NAME_ENGAGED", "Выбранное название занято.");
	define("ERR_NAME_EXCEEDS_AMOUNT", "Длина поискового запроса превышает значение ".STATION_MAX_LENGTH.".");

	/*
		Коды оповещений
	*/
	define('NOTIFY_BROADCAST_STOP', 'BROADCAST_STOP');
	define('NOTIFY_STATION_REMOVE', 'STATION_REMOVE');
	

	// массив текстов оповещений
	$NOTIFY_MESSAGES = array(
		NOTIFY_BROADCAST_STOP => 'Трансляция вашей станции остановлена. Добавьте треки в очередь и запустите трансляцию :)',
		NOTIFY_STATION_REMOVE => 'Через 30 дней ваши станции с остановленными трансляциями будут удалены. Чтобы этого не допустить, зайдите в панель управления станцией, добавьте треки в очередь и запустите трансляцию.',
	);