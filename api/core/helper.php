<?php if (!defined("API_ROOT")) exit("Hacking attempt!");

	// получает станцию по её названию
	function get_station_by_name($name)
	{
		global $db;

		$query = $db->prepare("SELECT * FROM stations WHERE name=?");
		$query->bind_param("s", $name);
		$query->execute();

		$result = $query->get_result();

		return ($result->num_rows > 0)?$result->fetch_assoc():NULL;
	}

	// получает станцию по её group_id
	function get_station_by_group_id($group_id, $with_owner = FALSE, $with_date = FALSE, $with_freezy = FALSE)
	{
		global $db;

		$query = $db->prepare("SELECT 
			stations.id, 
			stations.name, 
			genres.id AS genre_id, 
			genres.name AS genre_name, 
			stations.group_id,
			stations.rating
			".($with_owner?",stations.user_id":"")."
			".($with_date?",stations.date":"")."
			".($with_freezy?",stations.freezy":"")."
			FROM stations, genres WHERE 
			genres.id = stations.genre_id AND 
			stations.banned = 0 AND 
			stations.removed = 0 AND
			stations.group_id = ?");
		$query->bind_param("i", $group_id);
		$query->execute();

		$result = $query->get_result();

		return ($result->num_rows > 0)?$result->fetch_assoc():NULL;
	}

	// получает станцию по её id
	function get_station_by_id($station_id, $with_owner = FALSE)
	{
		global $db;

		$query = $db->prepare("SELECT 
			stations.id, 
			stations.name, 
			genres.id AS genre_id, 
			genres.name AS genre_name, 
			stations.group_id,
			stations.rating 
			".($with_owner?",stations.user_id":"")."
			FROM stations, genres WHERE 
			genres.id = stations.genre_id AND 
			stations.banned = 0 AND 
			stations.removed = 0 AND
			stations.id = ?");
		$query->bind_param("i", $station_id);
		$query->execute();

		$result = $query->get_result();

		return ($result->num_rows > 0)?$result->fetch_assoc():NULL;
	}

	// получает рандомную станцию
	function get_station_by_randid()
	{
		global $db;

		// получаем список id всех станций, из которую выберем рандомную
		$result = $db->query("SELECT id FROM stations WHERE banned = 0 AND removed = 0");
		$station_ids = $result->fetch_all(MYSQLI_ASSOC);

		sort($station_ids);

		$station_id_index = rand(0, count($station_ids));
		$station_id = (int)$station_ids[$station_id_index]["id"];

		unset($station_ids, $station_id_index);

		return get_station_by_id($station_id);
	}

	// получает список станций пользователя
	function get_user_stations($user_id)
	{
		global $db;

		$query = $db->prepare("SELECT
			id,
			name,
			group_id,
			date AS created_date
			FROM stations WHERE
			user_id = ?");
		$query->bind_param("i", $user_id);
		$query->execute();

		$result = $query->get_result();

		return ($result->num_rows > 0)?$result->fetch_all(MYSQLI_ASSOC):array();
	}

	// получает список жанров
	function get_genres()
	{
		global $db;

		$result = $db->query("SELECT id, name FROM genres ORDER BY position ASC");

		return ($result->num_rows > 0)?$result->fetch_all(MYSQLI_ASSOC):array();
	}

	// получает жанр по его id
	function get_genre_by_id($genre_id)
	{
		global $db;

		$query = $db->prepare("SELECT * FROM genres WHERE id=?");
		$query->bind_param("i", $genre_id);
		$query->execute();

		$result = $query->get_result();

		return ($result->num_rows > 0)?$result->fetch_assoc():NULL;
	}

	// получает список станций по поисковому запросу и id жанра
	function search_stations($q = "", $genre_id = 0, $station_ids = "", $runned = true)
	{
		global $db;

		$params = array();
		$sql = "SELECT 
			stations.id, 
			stations.name, 
			genres.id AS genre_id, 
			genres.name AS genre_name, 
			stations.group_id,
			stations.rating
			FROM stations, genres WHERE 
			genres.id = stations.genre_id AND 
			stations.banned = 0 AND 
			stations.removed = 0";
		if (strlen($station_ids) > 0)
			$sql .= " AND stations.id IN ($station_ids) ";
		if ($runned)
			$sql .= " AND stations.runned = 1";
		if ($genre_id > 1) {
			$sql .= " AND stations.genre_id = ? ";
			if (!empty($q)) {
				$q = "%$q%";
				$sql .= " AND stations.name LIKE ? ";
				$sql .= " ORDER BY sort DESC";
				$sql .= " LIMIT 0, ".MAX_SEARCH_COUNT;
				$query = $db->prepare($sql);
				$query->bind_param("is", $genre_id, $q);
			} else {
				$sql .= " ORDER BY sort DESC";
				$sql .= " LIMIT 0, ".MAX_SEARCH_COUNT;
				$query = $db->prepare($sql);
				$query->bind_param("i", $genre_id);
			}
		} else {
			if (!empty($q)) {
				$q = "%$q%";
				$sql .= " AND stations.name LIKE ? ";
				$sql .= " ORDER BY sort DESC";
				$sql .= " LIMIT 0, ".MAX_SEARCH_COUNT;
				$query = $db->prepare($sql);
				$query->bind_param("s", $q);
			} else {
				$sql .= " ORDER BY sort DESC";
				$sql .= " LIMIT 0, ".MAX_SEARCH_COUNT;
				$query = $db->prepare($sql);
			}
		}
		$query->execute();
		$result = $query->get_result();
		return ($result->num_rows > 0)?$result->fetch_all(MYSQLI_ASSOC):array();
	}

	// получает список треков из очереди станции
	function get_station_queue($station_id)
	{
		global $db;

		$query = $db->prepare("SELECT
								CONCAT(owner_id, '_', track_id) AS audio_id,
								duration
							   FROM queues
							   WHERE station_id=? AND
							   		 hide=0");
		$query->bind_param("i", $station_id);
		$query->execute();

		$result = $query->get_result();

		return ($result->num_rows > 0)?$result->fetch_all(MYSQLI_ASSOC):array();
	}

	// получает флаги станции: runned, banned, removed
	function get_station_flags($station_id)
	{
		global $db;

		$query = $db->prepare("SELECT runned,banned,removed FROM stations WHERE id=?");
		$query->bind_param("i", $station_id);
		$query->execute();
		$result = $query->get_result();

		return ($result->num_rows > 0)?$result->fetch_assoc():NULL;
	}

	// добавляет хит в статистику станции
	function add_station_hit($station_id, $user_id)
	{
		global $db;

		$query = $db->prepare("INSERT INTO stats(user_id,station_id) VALUES(?,?)");
		$query->bind_param("ii", $user_id, $station_id);

		return $query->execute();
	}

	// получает данные о текущем треке станции
	function get_current_track($station_id)
	{
		global $db;

		$query = $db->prepare("SELECT
							    CONCAT(queues.owner_id, '_', queues.track_id) AS audio_id,
							    queues.duration,
							    stations.end_time
							   FROM stations
							    LEFT JOIN queues ON queues.id=stations.track_id 
							   WHERE banned=0 
							    AND removed=0
							    AND runned=1 
							    AND stations.id=?");
		$query->bind_param("i", $station_id);
		$query->execute();
		$result = $query->get_result();

		if ($result->num_rows == 0)
			return NULL;

		$track = $result->fetch_assoc();

		if (is_null($track["audio_id"]))
			return NULL;

		return $track;
	}

	// перезаписывает треки в очереди
	function save_queue_tracks($station_id, $tracks)
	{
		global $db;

		$query = $db->prepare("INSERT INTO queues(track_id,owner_id,duration,station_id) VALUES(?,?,?,?)");
		$track_id = 0;
		$owner_id = 0;
		$duration = 0;
		$query->bind_param("iiii", $track_id, $owner_id, $duration, $station_id);

		$count = 0;

		foreach ($tracks as $track) {
			$audio_id = explode("_", $track["audio_id"]);

			$track_id = (int)$audio_id[1];
			$owner_id = (int)$audio_id[0];
			$duration = (int)$track["duration"];

			if (($track_id <= 0)
				|| ($owner_id == 0)
				|| ($duration <= 0))
				continue;

			if (FALSE === $query->execute()) {
				$query->close();
				return FALSE;
			} else
				$count++;
		}

		$query->close();

		return $count;
	}

	// удаляет все треки из очереди
	function remove_queue_tracks($station_id)
	{
		global $db;

		$query = $db->prepare("UPDATE queues SET hide=1 WHERE station_id=?");
		$query->bind_param("i", $station_id);

		return $query->execute();
	}

	// удаляет трек из очереди
	function remove_queue_track($id)
	{
		global $db;

		$query = $db->prepare("UPDATE queues SET hide=1 WHERE id=?");
		$query->bind_param("i", $id);

		return $query->execute();
	}

	// устанавливает текущий трек у станции
	function set_current_track($station_id, $track_id, $end_time)
	{
		global $db;

		$sql = "UPDATE stations SET track_id=?, end_time=? WHERE id=?";

		if ($track_id == 0 && $end_time == 0)
			$sql = "UPDATE stations SET track_id=?, end_time=?, runned=0 WHERE id=?";

		$query = $db->prepare($sql);
		$query->bind_param("iii", $track_id, $end_time, $station_id);

		return $query->execute();
	}

	// обновляет текущий трек у станции
	function update_current_track($station_id, $now)
	{
		global $db;

		$query = $db->prepare("SELECT
								id,
								track_id,
								CONCAT(owner_id, '_', track_id) AS audio_id,
								duration
							   FROM queues
							   WHERE station_id=?
							    AND hide=0 
							   LIMIT 0, 1");
		$query->bind_param("i", $station_id);
		$query->execute();
		$result = $query->get_result();

		if ($result->num_rows == 0) {
			set_current_track($station_id, 0, 0);
			return NULL;
		}

		$result = $result->fetch_assoc();
		$end_time = $now + $result["duration"];

		remove_queue_track($result["id"]);

		$tmp_time = time();

		// проверяет, существует ли во Вконтакте аудио с заданным audio_id
		if (FALSE === check_audio($result["audio_id"])) {
			$now += time() - $tmp_time;

			return update_current_track($station_id, $now);
		}

		set_current_track($station_id, $result["id"], $end_time);

		$track = array(
			"audio_id" => $result["audio_id"],
			"duration" => $result["duration"],
			"end_time" => $end_time,
		);

		return $track;
	}

	// проверяет существование аудио во ВКонтакте
	function check_audio($audio_id)
	{
		$result = vk_query('audio.getById', array('audios' => $audio_id), AUDIO_ACCESS_TOKEN);

		if (FALSE === array_key_exists('response', $result)) {
			return FALSE;
		}

		$result = $result['response'];
		$count = count($result);

		unset($result);

		return ($count > 0);
	}

	// получает данные голосования
	function get_rate($station_id, $user_id)
	{
		global $db;

		$query = $db->prepare("SELECT NULL FROM rating WHERE user_id=? AND station_id=?");
		$query->bind_param("ii", $user_id, $station_id);
		$query->execute();
		$result = $query->get_result();

		if ($result->num_rows > 0)
			return $result->fetch_assoc();
		else
			return NULL;
	}

	// удаляет голос с рейтинга станции
	function delete_rate($station_id, $user_id)
	{
		global $db;

		$query = $db->prepare("DELETE FROM rating WHERE user_id=? AND station_id=?");
		$query->bind_param("ii", $user_id, $station_id);
		$query->execute();

		$affected_rows = $query->affected_rows;

		unset($query);

		return ($affected_rows > 0);
	}

	// добавляет голос в рейтинг станции
	function rate_station($station_id, $user_id, $value)
	{
		global $db;

		$query = $db->prepare("INSERT INTO rating(user_id,station_id,value) VALUES(?,?,?)");
		$query->bind_param("iii", $user_id, $station_id, $value);

		return $query->execute();
	}

	// обновляет рейтинг станции
	function update_station_rating($station_id)
	{
		global $db;

		$query = $db->prepare("UPDATE stations SET rating=(SELECT AVG(value) FROM rating WHERE station_id=? AND hide=0) WHERE id=?");
		$query->bind_param("ii", $station_id, $station_id);
		$query->execute();

		$query = $db->prepare("SELECT rating FROM stations WHERE id=?");
		$query->bind_param("i", $station_id);
		$query->execute();

		$result = $query->get_result();

		return ($result->num_rows > 0)?$result->fetch_object()->rating:NULL;
	}

	// получает статистику по выбранному месяцу
	function get_station_stats_by_month($station_id, $year, $month)
	{
		global $db;

		$query = $db->prepare("SELECT DATE(date) AS day, COUNT(DISTINCT user_id) AS visitors FROM stats WHERE station_id=? AND YEAR(DATE)=? AND MONTH(date)=? GROUP BY DAY(date)");
		$query->bind_param("iii", $station_id, $year, $month);
		$query->execute();

		$result = $query->get_result();

		return ($result->num_rows > 0)?$result->fetch_all(MYSQLI_ASSOC):array();
	}

	// добавляет новую станцию
	function add_new_station($name, $genre_id, $group_id, $user_id)
	{
		global $db;

		$query = $db->prepare("INSERT INTO stations(name,genre_id,group_id,user_id) VALUES(?,?,?,?)");
		$query->bind_param("siii", $name, $genre_id, $group_id, $user_id);

		return $query->execute();
	}

	// редактирует параметры станции
	function edit_station($station_id, $name, $genre_id, $group_id)
	{
		global $db;

		$query = $db->prepare("UPDATE stations SET 
			name=?,
			genre_id=?,
			group_id=?
			WHERE id=?");
		$query->bind_param("siii", $name, $genre_id, $group_id, $station_id);

		return $query->execute();
	}

	// удаляет станцию пользователя
	function remove_user_station($station_id)
	{
		global $db;

		$query = $db->prepare("UPDATE stations SET removed=1, deleted_date=NOW(), track_id=0, end_time=0, rating=0, runned=0 WHERE id=?");
		$query->bind_param("i", $station_id);

		return $query->execute();
	}

	// устанавливает флаг runned у станции
	function set_station_runned($station_id, $runned)
	{
		global $db;

		$query = $db->prepare("UPDATE stations SET runned=? WHERE id=?");
		$query->bind_param("ii", $runned, $station_id);

		return $query->execute();
	}

	// возвращает user_id по station_id
	function get_user_id_by_station_id($station_id)
	{
		$station = get_station_by_id($station_id, TRUE);

		if (empty($station))
			return 0;

		return $station["user_id"];
	}

	// добавляет оповещение ведущему
	function add_notify($station_id, $message_code)
	{
		global $db;

		$user_id = get_user_id_by_station_id($station_id);

		if ($user_id == 0)
			return;

		$query = $db->prepare("INSERT IGNORE INTO notifications(user_id,message_code) VALUES(?,?)");
		$query->bind_param("is", $user_id, $message_code);

		return $query->execute();
	}

	// удаляет оповещение ведущего
	function clear_notify($station_id, $message_code)
	{
		global $db;

		$user_id = get_user_id_by_station_id($station_id);

		if ($user_id == 0)
			return;

		$query = $db->prepare("DELETE FROM notifications WHERE user_id=? AND message_code=?");
		$query->bind_param("is", $user_id, $message_code);

		return $query->execute();
	}

	// проверяет, есть ли оповещение у пользователя
	function has_notify($user_id, $message_code)
	{
		global $db;

		$query = $db->prepare("SELECT * FROM notifications WHERE user_id=? AND message_code=?");
		$query->bind_param("is", $user_id, $message_code);
		$query->execute();

		return $query->get_result()->num_rows > 0;
	}

	// отменяет удаление станции
	function deactivate_delete($station_id)
	{
		global $db;

		$query = $db->prepare("UPDATE stations SET deleted_date=NULL, removed=0 WHERE id=?");
		$query->bind_param("i", $station_id);

		return $query->execute();
	}

	// замораживает станцию
	function freezy_station($station_id)
	{
		global $db;

		$query = $db->prepare("UPDATE stations SET freezy=1 WHERE id=?");
		$query->bind_param("i", $station_id);

		return $query->execute();
	}

	// размораживает станцию
	function defreezy_station($station_id)
	{
		global $db;

		$query = $db->prepare("UPDATE stations SET freezy=0 WHERE id=?");
		$query->bind_param("i", $station_id);

		return $query->execute();
	}