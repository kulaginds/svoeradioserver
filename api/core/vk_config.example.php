<?php if (!defined("API_ROOT")) exit("Hacking attempt!");
	
	// параметры приложения ВК
	define("VK_APP_ID", 0);
	define("VK_APP_SECRET", "");

	// параметры панели ВК
	define("VK_PANEL_ID", 0);
	define("VK_PANEL_SECRET", "");

	// access_token для проверки существования аудио
	define("AUDIO_ACCESS_TOKEN", "");

	// версия VK API
	define('VK_VERSION', '5.52');
	// максимальное количество user_id для secure.sendNotifications
	define('VK_NOTIFY_COUNT', 100);
	