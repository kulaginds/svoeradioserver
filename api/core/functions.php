<?php if (!defined("API_ROOT")) exit("Hacking attempt!");
    
    // создает подключение к БД
    function connect_database()
    {
        if (!extension_loaded("mysqlnd"))
            print_error(DEBUG?ERR_NO_MYSQLND:ERR_INTERNAL, SYSTEM_ERROR);

        try {
            $link = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
            $link->set_charset(DB_CHARSET);

            $driver = new mysqli_driver();
            $driver->report_mode = MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT;
        } catch (mysqli_sql_exception $e) {
            print_error(DEBUG?ERR_CONN_DB:ERR_INTERNAL, SYSTEM_ERROR);
        }
        
        return $link;
    }

    // печатает результат в формате JSON
    function print_result($result)
    {
        print json_encode($result);
        die();
    }

    // формирует и печатает ошибку
    function print_error($message, $code = API_ERROR)
    {
        $error = array(
            "error" => array(
                "error_code" => $code,
                "error_msg" => $message,
            ),
        );
        print_result($error);
    }

    // формирует и печатает ответ
    function print_response($data)
    {
        $response = array(
            "response" => $data,
        );
        print_result($response);
    }

    // обработчик ошибок PHP
    function error_handler($errno, $errstr, $errfile, $errline, $errcontext)
    {
        print_error(ERR_INTERNAL, SYSTEM_ERROR);
    }

    // обработчик исключений PHP
    function exception_handler($e)
    {
        print_error(ERR_INTERNAL, SYSTEM_ERROR);
    }

    // проверяет пользовательскую авторизацию ВК
    function check_vk_auth($api_id, $auth_key, $viewer_id)
    {
        switch ($api_id) {
            case VK_APP_ID:
                $secret = VK_APP_SECRET;
                break;
            case VK_PANEL_ID:
                $secret = VK_PANEL_SECRET;
                break;
            default:
                return false;
                break;
        }

        $test_auth_key = md5($api_id."_".$viewer_id."_".$secret);

        return ($auth_key == $test_auth_key);
    }

    // получает параметр GET
    function get($name, $default = NULL)
    {
        return array_key_exists($name, $_GET)?$_GET[$name]:$default;
    }

    // получает параметр POST
    function post($name, $default = NULL)
    {
        return array_key_exists($name, $_POST)?$_POST[$name]:$default;
    }

    // проверяет, является ли числовым массивом
    function is_digit_array($array) {
        $is_digit = TRUE;
        foreach ($array as $item) {
            if (!ctype_digit($item)) {
                $is_digit = FALSE;
                break;
            }
        }
        return $is_digit;
    }

    // загружает данные по url и возвращает их в формате JSON
    function url_query($url, $method = 'GET', $data, $json_dec = true) {
        $data = http_build_query($data);
        if ($method == 'GET')
            $url .= '?'.$data;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 5);
        if ($method == 'POST') {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $answer = curl_exec($ch);
        
        if ($json_dec)
            $answer = json_decode($answer, true);

        curl_close($ch);

        return $answer;
    }

    // выполняет запрос к вк от имени сервера
    function vk_query($method, $params, $access_token = NULL) {
    	if (empty($access_token)) {
    		$access_token = url_query('https://oauth.vk.com/access_token', 'GET', array(
	            'client_id' => VK_APP_ID,
	            'client_secret' => VK_APP_SECRET,
	            'v' => VK_VERSION,
	            'grant_type' => 'client_credentials'
	        ))->access_token;
	        $params['client_secret'] = VK_APP_SECRET;
    	}

        $url = "https://api.vk.com/method/$method";
        $params['v'] = VK_VERSION;
        $params['access_token'] = $access_token;

        $result = url_query($url, 'POST', $params);

        return $result;
    }
