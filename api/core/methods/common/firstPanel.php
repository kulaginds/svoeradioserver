<?php if (!defined("API_ROOT")) exit("Hacking attempt!");
	
	include_once CORE_ROOT."/helper.php";

	$group_id = (int)post("group_id", 0);
	$viewer_id = (int)post("viewer_id", 0);

	if ($group_id <= 0)
		print_error(ERR_MISS_PARAM."group_id");

	$station = get_station_by_group_id($group_id, TRUE, TRUE, TRUE);

	if (is_null($station)) 
		print_error(ERR_STATION_NOT_EXISTS);

	if ($viewer_id != $station["user_id"])
		print_error(ERR_PERMISSIONS_DENIED);

	if (has_notify($viewer_id, NOTIFY_STATION_REMOVE)) {
		clear_notify($viewer_id, NOTIFY_STATION_REMOVE);
		deactivate_delete($station["id"]);
	}

	$queue = get_station_queue($station["id"]);
	$response = array(
		"genres" => get_genres(),
		"queue" => $queue,
		"params" => $station,
	);

	print_response($response);