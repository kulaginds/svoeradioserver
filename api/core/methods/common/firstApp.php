<?php if (!defined("API_ROOT")) exit("Hacking attempt!");
	
	include_once CORE_ROOT."/helper.php";
	
	$station_id = (int)post("station_id", 0);
	$group_id = (int)post("group_id", 0);
	$station_ids = post("station_ids", "");
	$viewer_id = (int)post("viewer_id", 0);

	if ($station_id < 0)
		print_error(ERR_MISS_PARAM."station_id");

	if ($group_id < 0)
		print_error(ERR_MISS_PARAM."group_id");

	if ((strlen($station_ids) > 0)) {
		$sids = explode(",", $station_ids);
		if (!is_digit_array($sids))
			print_error(ERR_MISS_PARAM."station_ids");
	}

	$is_group = FALSE;
	$station = NULL;

	if ($group_id > 0) {
		$group_station = get_station_by_group_id($group_id);
		
		if (!is_null($group_station)) {
			$is_group = TRUE;
			$station = $group_station;
		}
	}
	
	if (is_null($station))
		$station = get_station_by_id($station_id);

	if (is_null($station))
			$station = get_station_by_randid();

	$my = get_user_stations($viewer_id);

	$response = array(
		"genres" => get_genres(),
		"favorite" => search_stations("", 0, $station_ids, false),
		"stations" => search_stations(),
		"station" => $station,
		"isGroup" => (int)$is_group,
		"isOwner" => (count($my) > 0)?1:0,
		"my" => $my,
	);

	print_response($response);