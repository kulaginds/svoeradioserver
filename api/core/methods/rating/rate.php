<?php if (!defined("API_ROOT")) exit("Hacking attempt!");
	
	include_once CORE_ROOT."/helper.php";

	$station_id = (int)post("station_id", 0);
	$value = (int)post("value", 0);
	$viewer_id = (int)post("viewer_id", 0);

	if ($station_id <= 0)
		print_error(ERR_MISS_PARAM."station_id");

	if (($value <= 0)
		|| ($value > 5))
		print_error(ERR_MISS_PARAM."value");

	$station = get_station_by_id($station_id);

	if (is_null($station))
		print_error(ERR_STATION_NOT_EXISTS);

	$prev_voted = get_rate($station_id, $viewer_id);

	if (!is_null($prev_voted))
		//print_error(ERR_PREV_VOTED);
		delete_rate($station_id, $viewer_id);

	rate_station($station_id, $viewer_id, $value);

	$response = update_station_rating($station_id);

	print_response($response);