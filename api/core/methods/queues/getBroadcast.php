<?php if (!defined("API_ROOT")) exit("Hacking attempt!");
	
	include_once CORE_ROOT."/helper.php";
	
	$station_id = (int)post("station_id", 0);
	$viewer_id = (int)post("viewer_id", 0);

	if ($station_id <= 0)
		print_error(ERR_MISS_PARAM."station_id", API_ERROR_CRITICAL);

	$flags = get_station_flags($station_id);
	
	if (is_null($flags))
		print_error(ERR_STATION_NOT_EXISTS, API_ERROR_CRITICAL);

	if ($flags["runned"] == 0) {
		add_notify($station_id, NOTIFY_BROADCAST_STOP);
		print_error(ERR_BROADCAST_STOP, API_ERROR_CRITICAL);
	}

	if ($flags["banned"] == 1)
		print_error(ERR_STATION_BANNED, API_ERROR_CRITICAL);

	if ($flags["removed"] == 1)
		print_error(ERR_STATION_REMOVED, API_ERROR_CRITICAL);

	$track = get_current_track($station_id);
	$now = time();

	if (is_null($track)) {
		$track = update_current_track($station_id, $now);
		if (is_null($track)) {
			set_current_track($station_id, 0, 0);
			add_notify($station_id, NOTIFY_BROADCAST_STOP);
			print_error(ERR_BROADCAST_STOP, API_ERROR_CRITICAL);
		}
	}

	if ($track["end_time"] <= $now) {
		$track = update_current_track($station_id, $now);
		if (is_null($track)) {
			add_notify($station_id, NOTIFY_BROADCAST_STOP);
			print_error(ERR_BROADCAST_STOP, API_ERROR_CRITICAL);
		}
	}

	add_station_hit($station_id, $viewer_id);

	$time = $track["duration"] - ($track["end_time"] - $now);

	$response = array(
		"audio_id" => $track["audio_id"],
		"time" => $time,
	);

	print_response($response);