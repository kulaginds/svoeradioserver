<?php if (!defined("API_ROOT")) exit("Hacking attempt!");
	
	include_once CORE_ROOT."/helper.php";

	$station_id = (int)post("station_id", 0);
	$tracks = post("tracks");
	$viewer_id = (int)post("viewer_id", 0);

	$tracks = json_decode($tracks, TRUE);

	if ($station_id <= 0)
		print_error(ERR_MISS_PARAM."station_id");

	$station = get_station_by_id($station_id, TRUE);

	if ($viewer_id != $station["user_id"])
		print_error(ERR_PERMISSIONS_DENIED);

	if (is_null($tracks))
		print_error(ERR_MISS_PARAM."tracks");

	$count_tracks = count($tracks);

	if ($count_tracks > MAX_QUEUE_SIZE)
		print_error(ERR_QUEUE_EXCEEDS_AMOUNT);

	remove_queue_tracks($station_id);

	$count = save_queue_tracks($station_id, $tracks);

	if (FALSE === $count)
		print_error(ERR_QUEUE_SAVE);

	if ($count != $count_tracks)
		print_error(ERR_QUEUE_NOT_ALL_SAVED);

	print_response(1);