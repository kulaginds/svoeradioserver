<?php if (!defined("API_ROOT")) exit("Hacking attempt!");
	
	include_once CORE_ROOT."/helper.php";

	$station_id = (int)post("station_id", 0);
	$year = (int)post("year", 0);
	$month = (int)post("month", 0);
	$viewer_id = (int)post("viewer_id", 0);

	if ($station_id <= 0)
		print_error(ERR_MISS_PARAM."station_id");

	$current_year = (int)date("Y");
	$current_month = (int)date("m");

	if (($year < MIN_STATS_YEAR)
		|| ($year > $current_year))
		print_error(ERR_MISS_PARAM."year");

	if (($month <= 0)
		|| ($month > 12))
		print_error(ERR_MISS_PARAM."month");

	if (($year == $current_year)
		&& ($month > $current_month))
		print_error(ERR_MISS_LIM);

	$station = get_station_by_id($station_id, TRUE);

	if (is_null($station))
		print_error(ERR_STATION_NOT_EXISTS);

	if ($viewer_id != $station["user_id"])
		print_error(ERR_PERMISSIONS_DENIED);

	$response = get_station_stats_by_month($station_id, $year, $month);
	$end = cal_days_in_month(CAL_GREGORIAN, $month, $year);

	if (count($response) < $end) {
		for ($i = 1; $i < $end; $i++) {
			$day = $year."-".(($month <= 9)?"0".$month:$month)."-".(($i <= 9)?"0".$i:$i);

			foreach ($response as $item)
				if ($day == $item["day"])
					continue 2;
			
			$response[] = array(
				"day" => $day,
				"visitors" => 0,
			);
		}
	}


	print_response($response);