<?php if (!defined("API_ROOT")) exit("Hacking attempt!");

	include_once CORE_ROOT."/helper.php";

	$station_id = (int)post("station_id", 0);
	$name = post("name");
	$genre_id = (int)post("genre_id", 0);
	$group_id = (int)post("group_id", 0);
	$viewer_id = (int)post("viewer_id", 0);

	if ($station_id <= 0)
		print_error(ERR_MISS_PARAM."station_id");

	$lenname = mb_strlen($name, "UTF-8");

	if (($lenname < STATION_MIN_LENGTH)
		|| ($lenname > STATION_MAX_LENGTH))
		print_error(ERR_MISS_PARAM."name");

	if ($genre_id <= 1)
		print_error(ERR_MISS_PARAM."genre_id");

	if ($group_id <= 0)
		print_error(ERR_MISS_PARAM."group_id");

	$station = get_station_by_id($station_id, TRUE);

	if (is_null($station))
		print_error(ERR_STATION_NOT_EXISTS);

	if ($viewer_id != $station["user_id"])
		print_error(ERR_PERMISSIONS_DENIED);

	$tmp = get_station_by_name($name);

	if (!is_null($tmp)
		&& ($tmp["id"] != $station["id"]))
		print_error(ERR_NAME_ENGAGED);
	
	if (is_null(get_genre_by_id($genre_id)))
		print_error(ERR_MISS_PARAM."genre_id");

	$tmp = get_station_by_group_id($group_id);

	if (!is_null($tmp)
		&& ($tmp["id"] != $station["id"]))
		print_error(ERR_GROUP_ENGAGED);

	$response = edit_station($station_id, $name, $genre_id, $group_id, $viewer_id)?1:0;

	print_response($response);