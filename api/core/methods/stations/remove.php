<?php if (!defined("API_ROOT")) exit("Hacking attempt!");
	
	include_once CORE_ROOT."/helper.php";
	
	$station_id = (int)post("station_id", 0);
	$viewer_id = (int)post("viewer_id", 0);

	if ($station_id <= 0)
		print_error(ERR_MISS_PARAM."station_id");

	$station = get_station_by_id($station_id, TRUE);

	if (is_null($station))
		print_error(ERR_STATION_NOT_EXISTS);

	if ($viewer_id != $station["user_id"])
		print_error(ERR_PERMISSIONS_DENIED);

	$response = remove_user_station($station_id)?1:0;

	print_response($response);