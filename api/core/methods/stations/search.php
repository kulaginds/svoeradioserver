<?php if (!defined("API_ROOT")) exit("Hacking attempt!");
	
	include_once CORE_ROOT."/helper.php";
	
	$q = post("q", "");
	$genre_id = (int)post("genre_id", 0);
	$station_ids = post("station_ids", "");

	// чистка $q
	$q = htmlspecialchars(trim($q));

	$lenq = mb_strlen($q, "UTF-8");

	if ($lenq > STATION_MAX_LENGTH)
		print_error(ERR_NAME_EXCEEDS_AMOUNT);

	if ($genre_id < 0)
		print_error(ERR_MISS_PARAM."genre_id");

	if ($genre_id > 0) {
		$genre = get_genre_by_id($genre_id);
		if (is_null($genre))
			print_error(ERR_MISS_PARAM."genre_id");
	}

	$runned = true;

	if ((strlen($station_ids) > 0)) {
		$sids = explode(",", $station_ids);
		if (!is_digit_array($sids))
			print_error(ERR_MISS_PARAM."station_ids");
		$runned = !$runned;
	}
	
	$response = search_stations($q, $genre_id, $station_ids, $runned);

	print_response($response);