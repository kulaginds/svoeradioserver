<?php if (!defined("API_ROOT")) exit("Hacking attempt!");
	
	include_once CORE_ROOT."/helper.php";

	$station_id = (int)post("station_id", 0);
	$runned = (int)post("runned", 0);
	$viewer_id = (int)post("viewer_id", 0);

	if ($station_id <= 0)
		print_error(ERR_MISS_PARAM."station_id");

	if (($runned < 0)
		|| ($runned > 1))
		print_error(ERR_MISS_PARAM."runned");

	$station = get_station_by_id($station_id, TRUE);

	if (is_null($station))
		print_error(ERR_STATION_NOT_EXISTS);

	if ($viewer_id != $station["user_id"])
		print_error(ERR_PERMISSIONS_DENIED);

	$response = set_station_runned($station_id, $runned)?1:0;

	if ($runned == 1)
		if (is_null(update_current_track($station_id, time())))
			print_error(ERR_NO_QUEUE_TRACKS);
		else
			clear_notify($station_id, NOTIFY_BROADCAST_STOP);

	print_response($response);