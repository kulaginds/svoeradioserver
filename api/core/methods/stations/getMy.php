<?php if (!defined("API_ROOT")) exit("Hacking attempt!");
	
	include_once CORE_ROOT."/helper.php";
	
	$viewer_id = (int)post("viewer_id", 0);

	$response = get_user_stations($viewer_id);

	print_response($response);