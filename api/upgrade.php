<?php

	define("API_ROOT", dirname(__FILE__));
	define("CORE_ROOT", API_ROOT."/core");
	
	header("Content-type: text/html; charset=utf-8");

	include_once CORE_ROOT."/config.php";
	include_once CORE_ROOT."/functions.php";
	include_once CORE_ROOT."/helper.php";

	if (!DEBUG)
		exit("disabled by admin");

	$db = connect_database();

	$welcome = "part 1: add to table queues columns: owner_id, duration<br>
	part 2: copy data from tracks to queues<br>
	part 3: delete table tracks<br>
	part 4: update current track for all running stations";
	$welcome .= "<hr><a href=\"upgrade.php?part=%d\">Run part %d</a><hr>";

	function copyData()
	{
		global $db;

		$query = $db->query("SELECT id AS track_id, owner_id, duration FROM tracks");
		$tmp = $query->fetch_all(MYSQLI_ASSOC);
		$tracks = array(); // здесь будут храниться все треки

		foreach ($tmp as $track)
		{
			$tracks[$track["track_id"]] = $track;
		}

		unset($query, $tmp, $track);

		$query = $db->query("SELECT id, track_id FROM queues");
		$update = $db->prepare("UPDATE queues SET owner_id=?, duration=? WHERE id=?");
		
		while ($row = $query->fetch_object())
		{
			$track = $tracks[$row->track_id];
			$update->bind_param("iii", $track["owner_id"], $track["duration"], $row->id);
			$update->execute();
		}

		$query->free();

		unset($query, $update, $row, $track);
	}

	function updateCurrentTrack()
	{
		global $db;

		$query = $db->query("SELECT id FROM stations WHERE banned=0 AND removed=0 AND runned=1");
		$now = time();

		while ($row = $query->fetch_object())
		{
			update_current_track($row->id, $now);
		}
	}

	if (isset($_GET["part"])) {
		switch ($_GET["part"]) {
			case 1:
				$db->query("ALTER TABLE queues ADD owner_id INT(10) NOT NULL AFTER track_id, ADD duration INT(10) UNSIGNED NOT NULL AFTER owner_id;");
				printf($welcome, 2, 2);
				break;
			case 2:
				copyData();
				printf($welcome, 3, 3);
				break;
			case 3:
				$db->query("DROP TABLE tracks;");
				printf($welcome, 4, 4);
				break;
			case 4:
				updateCurrentTrack();
				echo "Done!";
				break;
			
			default:
				printf($welcome, 1, 1);
				break;
		}
	} else
		printf($welcome, 1, 1);

?>